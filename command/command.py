class Instalador:

    def __init__(self, fonte, destino):
        self.opcoes = []
        self.destino = destino
        self.fonte = fonte

    def preferencias(self, escolha):
        self.opcoes.append(escolha)

    def executar(self):
        for opc in self.opcoes:
            if list(opc.values())[0]:
                print(
                    f'Copiando os binários de {self.fonte} para {self.destino}')
            else:
                print('Operação Finalizada')


if __name__ == '__main__':
    instalador = Instalador('python3.9.1.gz', '/usr/bin/')

    instalador.preferencias({'python': True})
    instalador.preferencias({'java': False})

    instalador.executar()