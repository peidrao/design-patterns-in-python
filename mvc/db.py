import sqlite3
from sqlite3.dbapi2 import Cursor


def _executar(query):
    db_path = './sqlite3.db'
    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()
    resultado = None

    try:
        cursor.execute(query)
        resultado = cursor.fetchall()
        connection.commit()
    except Exception as err:
        print(f'Erro na execução da query: {err}')
    finally:
        connection.close()

    return resultado
