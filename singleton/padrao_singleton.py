# O __new__ é executado antes do construtor __init__

class Singleton(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
            print(f'Criando objeto {cls.instance}')
        return cls.instance


s1 = Singleton()
print(f'Instância: {id(s1)}')

s2 = Singleton()
print(f'Instância: {id(s2)}')