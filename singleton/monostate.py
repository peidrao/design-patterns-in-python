class Monosate:
    __estado = {}

    def __new__(cls, *args, **kwargs):
        obj = super(Monosate, cls).__new__(cls, *args, **kwargs)
        obj.__dict__ = cls.__estado
        return obj


m1 = Monosate()
print(f'M1 ID: {id(m1)}')
print(m1.__dict__)

m2 = Monosate()
print(f'M2 ID: {id(m2)}')
print(m2.__dict__)

m1.nome = 'Maria'

print(f'M1: {m1.__dict__}')
print(f'M2: {m2.__dict__}')
