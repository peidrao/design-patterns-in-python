class University(type):
    def __call__(cls, *args, **kwargs):
        print(f'ARGUMENTOS: {args}')
        return type.__call__(cls, *args, **kwargs)


class Geek(metaclass=University):
    def __init__(self, val1, val2):
        self.valor1 = val1
        self.valor2 = val2


obj = Geek(50, 100)
print(obj)