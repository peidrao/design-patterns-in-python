from abc import ABCMeta


class State(metaclass=ABCMeta):
    def manipular(self):
        pass
        

class StateConcretoA(State):
    def manipular(self):
        print('StateConcretaA')


class StateConcretoB(State):
    def manipular(self):
        print('StateConcretaB')


class Context(State):
    def __init__(self):
        self.state = None

    def get_state(self):
        return self.state

    def set_state(self, state):
        self.state = state

    def manipular(self):
        self.state.manipular()


if __name__ == '__main__':
    contexto = Context()
    stateA = StateConcretoA()
    stateB = StateConcretoB()

    contexto.set_state(stateA)
    contexto.manipular()

    contexto.set_state(stateB)
    contexto.manipular()
