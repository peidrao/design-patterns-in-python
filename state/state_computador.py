from abc import ABCMeta


class State(metaclass=ABCMeta):
    nome = 'estado'
    permitido = []

    def mudar(self, estado):
        if estado.nome in self.permitido:
            print(f'Autal: {self} => Mudado para o estado: {estado.nome}')
            self.__class__ = estado
        else:
            print(f'Atual: {self} => Não é possível mudar')

    
    def __str__(self) -> str:
        return self.nome
        

class Ligar(State):
    nome = 'Ligar'
    permitido = ['Desligar', 'Suspender', 'Hibernar']
    

class Desligar(State):
    nome = 'Desligar'
    permitido = ['Ligar']


class Suspender(State):
    nome = 'Suspender'
    permitido = ['Ligar']

class Hibernar(State):
    nome = 'Hibernar'
    permitido = ['Ligar']


class StateConcretoB(State):
    def manipular(self):
        print('StateConcretaB')


class Computador:
    def __init__(self, modelo='Dell'):
        self.modelo = modelo
        self.estado = Desligar()

    def alterar(self, estado):
        self.estado.mudar(estado)


if __name__ == '__main__':
    computador = Computador()
    
    # Ligar
    computador.alterar(Ligar)
    
    # Desligar
    computador.alterar(Desligar)

    # Ligar
    computador.alterar(Ligar)

    # Suspender
    computador.alterar(Suspender)

    # Tentar Hibernar
    computador.alterar(Hibernar)

    # Ligar
    computador.alterar(Ligar)
